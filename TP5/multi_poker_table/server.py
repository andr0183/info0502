import paho.mqtt.client as mqtt
import json

tables = {}

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("poker/game/#")

def on_message(client, userdata, msg):
    message = json.loads(msg.payload)
    table_id = msg.topic.split("/")[-1]

    if message["action"] == "create_table":
        tables[table_id] = {"admin": message["player"], "players": []}
        print(f"Table {table_id} created by {message['player']}.")
    elif message["action"] == "join_table":
        if table_id in tables and len(tables[table_id]["players"]) < 8:
            tables[table_id]["players"].append(message["player"])
            print(f"Player {message['player']} joined table {table_id}.")
        else:
            print(f"Player {message['player']} cannot join table {table_id}.")
    elif message["action"] == "close_table":
        if table_id in tables and tables[table_id]["admin"] == message["player"]:
            del tables[table_id]
            print(f"Table {table_id} closed by {message['player']}.")
        else:
            print(f"Player {message['player']} cannot close table {table_id}.")
    # Ajoutez d'autres actions ici

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("localhost", 1883, 60)

client.loop_forever()
