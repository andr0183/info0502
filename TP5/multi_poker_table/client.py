import paho.mqtt.client as mqtt
import json

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

client = mqtt.Client()
client.on_connect = on_connect

client.connect("adresse_ip_infrastructure", 1883, 60)

client.loop_start()

# Exemple de message pour créer une table
message = {"action": "create_table", "player": "Player1"}
client.publish("poker/game/table1", json.dumps(message))

# Exemple de message pour rejoindre une table
message = {"action": "join_table", "player": "Player2"}
client.publish("poker/game/table1", json.dumps(message))

# Exemple de message pour fermer une table
message = {"action": "close_table", "player": "Player1"}
client.publish("poker/game/table1", json.dumps(message))

client.loop_stop()
client.disconnect()
