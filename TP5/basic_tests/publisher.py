import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

client = mqtt.Client()
client.on_connect = on_connect

client.connect("adresse_ip_infrastructure", 1883, 60)

client.loop_start()

client.publish("test/topic", "Hello MQTT")

client.loop_stop()
client.disconnect()
