import paho.mqtt.client as mqtt
import json

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

client = mqtt.Client()
client.on_connect = on_connect

client.connect("adresse_ip_infrastructure", 1883, 60)

client.loop_start()

# Exemple de message pour rejoindre la partie
message = {"action": "join", "player": "Player1"}
client.publish("poker/game", json.dumps(message))

# Exemple de message pour miser
message = {"action": "bet", "player": "Player1", "amount": 100}
client.publish("poker/game", json.dumps(message))

client.loop_stop()
client.disconnect()
