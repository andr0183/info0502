import paho.mqtt.client as mqtt
import json

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("poker/game")

def on_message(client, userdata, msg):
    message = json.loads(msg.payload)
    if message["action"] == "join":
        print(f"Player {message['player']} joined the game.")
    elif message["action"] == "bet":
        print(f"Player {message['player']} bets {message['amount']}.")
    # Ajoutez d'autres actions ici

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("localhost", 1883, 60)

client.loop_forever()
