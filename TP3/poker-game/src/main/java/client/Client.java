// Client.java
import java.io.*;
import java.net.*;
import java.util.List;

public class Client {
    private static final String SERVER_ADDRESS = "localhost";
    private static final int SERVER_PORT = 12345;

    public static void main(String[] args) {
        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT)) {
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());

            String nickname = "Player1"; // Replace with user input
            output.writeObject(nickname);
            String response = (String) input.readObject();
            System.out.println(response);

            List<Card> cards = (List<Card>) input.readObject();
            System.out.println("Your cards: " + cards);

            List<Card> communityCards = (List<Card>) input.readObject();
            System.out.println("Community cards: " + communityCards);

            // Implement receiving results and winner announcement
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Client exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
