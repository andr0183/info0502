// Server.java
import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    private static final int PORT = 12345;
    private static final int MAX_PLAYERS = 10;
    private static List<Player> players = new ArrayList<>();
    private static Deck deck = new Deck();
    private static List<Card> communityCards = new ArrayList<>();

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server is listening on port " + PORT);
            while (true) {
                Socket socket = serverSocket.accept();
                new ServerThread(socket).start();
            }
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private static class ServerThread extends Thread {
        private Socket socket;
        private ObjectInputStream input;
        private ObjectOutputStream output;

        public ServerThread(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            try {
                input = new ObjectInputStream(socket.getInputStream());
                output = new ObjectOutputStream(socket.getOutputStream());

                String nickname = (String) input.readObject();
                Player player = new Player(nickname);
                players.add(player);
                output.writeObject("Connected as " + nickname);

                if (players.size() >= 2) {
                    startGame();
                }
            } catch (IOException | ClassNotFoundException ex) {
                System.out.println("Server thread exception: " + ex.getMessage());
                ex.printStackTrace();
            }
        }

        private void startGame() {
            distributeCards();
            distributeFlop();
            distributeTurn();
            distributeRiver();
            evaluateHands();
            sendResults();
            determineWinner();
        }

        private void distributeCards() {
            for (Player player : players) {
                List<Card> cards = deck.drawCards(2);
                player.setCards(cards);
                sendCardsToPlayer(player, cards);
            }
        }

        private void distributeFlop() {
            communityCards.addAll(deck.drawCards(3));
            sendCommunityCardsToAllPlayers();
        }

        private void distributeTurn() {
            communityCards.add(deck.drawCard());
            sendCommunityCardsToAllPlayers();
        }

        private void distributeRiver() {
            communityCards.add(deck.drawCard());
            sendCommunityCardsToAllPlayers();
        }

        private void evaluateHands() {
            // Implement hand evaluation logic
        }

        private void sendResults() {
            // Implement sending results to all players
        }

        private void determineWinner() {
            // Implement determining the winner
        }

        private void sendCardsToPlayer(Player player, List<Card> cards) {
            try {
                output.writeObject(cards);
            } catch (IOException ex) {
                System.out.println("Error sending cards to player: " + ex.getMessage());
                ex.printStackTrace();
            }
        }

        private void sendCommunityCardsToAllPlayers() {
            for (Player player : players) {
                try {
                    player.getSocket().getOutputStream().writeObject(communityCards);
                } catch (IOException ex) {
                    System.out.println("Error sending community cards to player: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }
    }
}

// Player.java
import java.io.Serializable;
import java.net.Socket;
import java.util.List;

public class Player implements Serializable {
    private String nickname;
    private List<Card> cards;
    private transient Socket socket;

    public Player(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}

// Card.java
import java.io.Serializable;

public class Card implements Serializable {
    private String value;
    private String suit;

    public Card(String value, String suit) {
        this.value = value;
        this.suit = suit;
    }

    public String getValue() {
        return value;
    }

    public String getSuit() {
        return suit;
    }
}

// Deck.java
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    private List<Card> cards;

    public Deck() {
        cards = new ArrayList<>();
        String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        for (String suit : suits) {
            for (String value : values) {
                cards.add(new Card(value, suit));
            }
        }
        Collections.shuffle(cards);
    }

    public List<Card> drawCards(int number) {
        List<Card> drawnCards = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            drawnCards.add(cards.remove(0));
        }
        return drawnCards;
    }

    public Card drawCard() {
        return cards.remove(0);
    }
}
