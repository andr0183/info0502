import java.io.Serializable;
import java.util.List;

public class Player implements Serializable {
    private String nickname;
    private List<Card> cards;

    public Player(String nickname) {
        this.nickname = nickname;
        this.cards = new ArrayList<>();
    }

    public String getNickname() {
        return nickname;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}