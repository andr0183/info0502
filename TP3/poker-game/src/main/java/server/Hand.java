import java.io.Serializable;
import java.util.List;

public class Hand implements Serializable {
    private List<Card> cards;

    public Hand(List<Card> cards) {
        this.cards = cards;
    }

    public List<Card> getCards() {
        return cards;
    }

    public String evaluateHand() {
        // Implement hand evaluation logic
        return "Hand evaluation result";
    }
}