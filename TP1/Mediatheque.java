import java.util.Vector;

public class Mediatheque {
    private String nomProprietaire;
    private Vector<Media> medias;

    // Constructeur par défaut
    public Mediatheque() {
        this.nomProprietaire = "";
        this.medias = new Vector<>();
    }

    // Constructeur par copie
    public Mediatheque(Mediatheque other) {
        this.nomProprietaire = other.nomProprietaire;
        this.medias = new Vector<>(other.medias);
    }

    // Méthode pour ajouter un média
    public void add(Media media) {
        medias.add(media);
    }

    @Override
    public String toString() {
        return "Propriétaire: " + nomProprietaire + ", Médias: " + medias.toString();
    }
}

