public class Livre extends Media {
    private String auteur;
    private String isbn;

    // Constructeur par défaut
    public Livre() {
        super();
        this.auteur = "";
        this.isbn = "";
    }

    // Constructeur par initialisation
    public Livre(String titre, StringBuffer cote, int note, String auteur, String isbn) {
        super(titre, cote, note);
        this.auteur = auteur;
        this.isbn = isbn;
    }

    // Constructeur par copie
    public Livre(Livre other) {
        super(other);
        this.auteur = other.auteur;
        this.isbn = other.isbn;
    }

    // Getters et setters pour auteur et isbn
    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return super.toString() + ", Auteur: " + auteur + ", ISBN: " + isbn;
    }
}

