public class Film extends Media {
    private String realisateur;
    private int annee;

    // Constructeur par défaut
    public Film() {
        super();
        this.realisateur = "";
        this.annee = 0;
    }

    // Constructeur par initialisation
    public Film(String titre, StringBuffer cote, int note, String realisateur, int annee) {
        super(titre, cote, note);
        this.realisateur = realisateur;
        this.annee = annee;
    }

    // Constructeur par copie
    public Film(Film other) {
        super(other);
        this.realisateur = other.realisateur;
        this.annee = other.annee;
    }

    // Getters et setters pour realisateur et annee
    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public String toString() {
        return super.toString() + ", Réalisateur: " + realisateur + ", Année: " + annee;
    }
}

