public class Media {
    // Données d'instance privées
    private String titre;
    private StringBuffer cote;
    private int note;

    // Donnée de classe privée
    private static String nomMediatheque;

    // Constructeur par défaut
    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    // Constructeur par initialisation
    public Media(String titre, StringBuffer cote, int note) {
        this.titre = titre;
        this.cote = cote;
        this.note = note;
    }

    // Constructeur par copie
    public Media(Media other) {
        this.titre = other.titre;
        this.cote = new StringBuffer(other.cote);
        this.note = other.note;
    }

    // Getters et setters pour les données d'instance
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return new StringBuffer(cote); // Crée une copie de l'objet pour éviter la modification directe
    }

    public void setCote(StringBuffer cote) {
        this.cote = cote;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    // Méthodes de classe pour accéder/modifier nomMediatheque
    public static String getNomMediatheque() {
        return nomMediatheque;
    }

    public static void setNomMediatheque(String nom) {
        nomMediatheque = nom;
    }

    // Masquage des méthodes clone, equals, et toString
    @Override
    public String toString() {
        return "Titre: " + titre + ", Cote: " + cote + ", Note: " + note;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media media = (Media) obj;
        return note == media.note && titre.equals(media.titre) && cote.toString().equals(media.cote.toString());
    }

    @Override
    public Media clone() {
        return new Media(this);
    }
}

