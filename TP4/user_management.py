import pika
import json

# Connexion à RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Déclaration des files
channel.queue_declare(queue='user_registration_queue')

# Fonction pour enregistrer un utilisateur
def register_user(username, password):
    message = {"operation": "register", "username": username, "password": password}
    channel.basic_publish(exchange='', routing_key='user_registration_queue', body=json.dumps(message))
    print(f"User {username} registered.")

# Fermeture de la connexion
connection.close()
