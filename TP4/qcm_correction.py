import pika
import json

# Connexion à RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Déclaration des files
channel.queue_declare(queue='qcm_correction_queue')

# Fonction pour corriger un QCM
def correct_qcm(username, qcm_id, answers):
    message = {"operation": "correct_qcm", "username": username, "qcm_id": qcm_id, "answers": answers}
    channel.basic_publish(exchange='', routing_key='qcm_correction_queue', body=json.dumps(message))
    print(f"QCM {qcm_id} corrected for {username}.")

# Fermeture de la connexion
connection.close()
