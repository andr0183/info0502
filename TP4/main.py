from user_management import register_user
from qcm_management import request_qcm, distribute_qcm
from qcm_correction import correct_qcm

# Exemple d'utilisation
if __name__ == "__main__":
    # Inscription d’un utilisateur
    register_user("user1", "password1")

    # Demande d’un QCM
    request_qcm("user1", "qcm1")

    # Distribution des questions
    questions = [
        {"question": "Quelle est la capitale de la France ?", "options": ["Paris", "Londres", "Berlin", "Madrid"]},
        {"question": "Quelle est la formule de l'eau ?", "options": ["H2O", "CO2", "O2", "N2"]}
    ]
    distribute_qcm("user1", "qcm1", questions)

    # Correction d’un QCM
    answers = [
        {"question": "Quelle est la capitale de la France ?", "answer": "Paris"},
        {"question": "Quelle est la formule de l'eau ?", "answer": "H2O"}
    ]
    correct_qcm("user1", "qcm1", answers)
