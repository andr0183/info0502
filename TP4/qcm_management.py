import pika
import json

# Connexion à RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Déclaration des files
channel.queue_declare(queue='qcm_request_queue')
channel.queue_declare(queue='qcm_distribution_queue')

# Fonction pour demander un QCM
def request_qcm(username, qcm_id):
    message = {"operation": "request_qcm", "username": username, "qcm_id": qcm_id}
    channel.basic_publish(exchange='', routing_key='qcm_request_queue', body=json.dumps(message))
    print(f"QCM {qcm_id} requested by {username}.")

# Fonction pour distribuer un QCM
def distribute_qcm(username, qcm_id, questions):
    message = {"operation": "distribute_qcm", "username": username, "qcm_id": qcm_id, "questions": questions}
    channel.basic_publish(exchange='', routing_key='qcm_distribution_queue', body=json.dumps(message))
    print(f"QCM {qcm_id} distributed to {username}.")

# Fermeture de la connexion
connection.close()
