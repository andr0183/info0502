package edu.info0502.tp1;

import java.util.Vector;

public class Mediatheque {
    private String proprietaire;
    private Vector<Media> medias;

    public Mediatheque(String proprietaire) {
        this.proprietaire = proprietaire;
        this.medias = new Vector<>();
    }

    public void add(Media media) {
        medias.add(media);
    }

    @Override
    public String toString() {
        return "Mediatheque de " + proprietaire + " avec " + medias.size() + " médias :\n" + medias;
    }
}

