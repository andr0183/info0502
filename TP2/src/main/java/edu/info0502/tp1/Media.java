package edu.info0502.tp1;

public class Media {
    private String titre;
    private StringBuffer cote;
    private int note;
    private static String nomMediatheque = "Mediatheque Default";

    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    public Media(Media media) {
        this.titre = media.titre;
        this.cote = new StringBuffer(media.cote.toString());
        this.note = media.note;
    }

    public String getTitre() { return titre; }
    public void setTitre(String titre) { this.titre = titre; }

    public StringBuffer getCote() { return new StringBuffer(cote); }
    public void setCote(String cote) { this.cote = new StringBuffer(cote); }

    public int getNote() { return note; }
    public void setNote(int note) { this.note = note; }

    public static String getNomMediatheque() { return nomMediatheque; }
    public static void setNomMediatheque(String nom) { nomMediatheque = nom; }

    @Override
    public Media clone() { return new Media(this); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Media)) return false;
        Media media = (Media) o;
        return note == media.note && titre.equals(media.titre) && cote.toString().equals(media.cote.toString());
    }

    @Override
    public String toString() {
        return "Media [titre=" + titre + ", cote=" + cote + ", note=" + note + "]";
    }
}

